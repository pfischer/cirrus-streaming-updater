title: cirrussearch/update_pipeline/fetch_failure
description: |
  Indicates a failure when fetching the CirrusSearch document from mediawiki
$id: /cirrussearch/update_pipeline/fetch_failure/1.0.0
$schema: 'https://json-schema.org/draft-07/schema#'
type: object
additionalProperties: false
required:
  - $schema
  - database
  - dt
  - error_message
  - error_type
  - meta
  - original_event_stream
  - original_event_time
  - original_ingestion_time
  - page_id
  - page_namespace
  - page_title
  - pipeline
properties:
  $schema:
    description: >
      A URI identifying the JSONSchema for this event. This should match an
      schema's $id in a schema repository. E.g. /schema/title/1.0.0
    type: string
  database:
    description: >-
      The name of the wiki database that the event causing the failure belongs
      to.
    type: string
    minLength: 1
  dt:
    description: >
      ISO-8601 formatted timestamp of when the event occurred/was generated in
      UTC), AKA 'event time'. This is different than meta.dt, which is used as
      the time the system received this event.
    type: string
    format: date-time
    maxLength: 128
  error_message:
    description: The name of the wiki database this event entity belongs to.
    type: string
    minLength: 1
  error_type:
    description: The name of the wiki database this event entity belongs to.
    type: string
    enum:
      - NOT_FOUND
      - NETWORK_ERROR
      - MW_ERROR
  meta:
    type: object
    required:
      - stream
    properties:
      domain:
        description: Domain the event or entity pertains to
        type: string
        minLength: 1
      dt:
        description: 'Time the event was received by the system, in UTC ISO-8601 format'
        type: string
        format: date-time
        maxLength: 128
      id:
        description: Unique ID of this event
        type: string
      request_id:
        description: Unique ID of the request that caused the event
        type: string
      stream:
        description: Name of the stream (dataset) that this event belongs in
        type: string
        minLength: 1
      uri:
        description: Unique URI identifying the event or entity
        type: string
        format: uri-reference
        maxLength: 8192
  original_event_stream:
    description: Name of the stream (dataset) that the event causing the failure belongs in
    type: string
    minLength: 1
  original_event_time:
    description: >
      ISO-8601 formatted timestamp of when the event causing the failure
      occurred/was generated in UTC), AKA 'event time'.
    type: string
    format: date-time
    maxLength: 128
  original_ingestion_time:
    description: >-
      ISO-8601 formatted timestamp of when the event causing the failure entered
      the pipeline.
    type: string
    format: date-time
    maxLength: 128
  page_id:
    description: The (database) page ID.
    type: integer
    maximum: 9007199254740991
    minimum: 0
  page_namespace:
    description: The namespace ID this page belongs to.
    type: integer
    maximum: 9007199254740991
    minimum: -9007199254740991
  page_title:
    description: The normalized title of the page.
    type: string
    minLength: 1
  pipeline:
    description: The name of the CirrusSearch update pipeline.
    type: string
    minLength: 1
  rev_id:
    description: The (database) revision ID.
    type: integer
    maximum: 9007199254740991
    minimum: 0
examples:
  - $schema: /cirrussearch/update_pipeline/fetch_failure/1.0.0
    database: testwiki
    dt: '2022-11-11T00:00:00Z'
    error_message: Cannot find page Test page
    error_type: NOT_FOUND
    meta:
      domain: test.wikipedia.org
      dt: '2022-11-11T00:00:10Z'
      stream: cirrussearch-update-pipeline-fetch-failure
    original_event_stream: mediawiki.page-state
    original_event_time: '2022-11-11T00:00:00Z'
    original_ingestion_time: '2022-11-11T00:00:05Z'
    page_id: 123
    page_namespace: 0
    page_title: Test page
    pipeline: cirrussearch-beta-eqiad
