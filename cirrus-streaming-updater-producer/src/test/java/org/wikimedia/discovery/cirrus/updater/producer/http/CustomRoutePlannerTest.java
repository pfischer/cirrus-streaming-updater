package org.wikimedia.discovery.cirrus.updater.producer.http;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.apache.hc.client5.http.HttpRoute;
import org.apache.hc.client5.http.impl.DefaultSchemePortResolver;
import org.apache.hc.client5.http.impl.routing.DefaultRoutePlanner;
import org.apache.hc.client5.http.routing.HttpRoutePlanner;
import org.apache.hc.core5.http.HttpException;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.http.protocol.BasicHttpContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CustomRoutePlannerTest {

    Map<String, HttpHost> customRouteMapTest = new HashMap<>();
    HttpRoutePlanner defaultRoutePlanner =
            new DefaultRoutePlanner(DefaultSchemePortResolver.INSTANCE);

    @BeforeEach
    public void setUp() {
        customRouteMapTest.put("managed.test", new HttpHost("someScheme", "route.test", 9999));
        customRouteMapTest.put("proxy.test", new HttpHost("diffScheme", "route.test", 8080));
    }

    @Test
    void shouldNotGetSameHost() throws HttpException {
        Map<String, HttpHost> emptyMap = new HashMap<>();
        CustomRoutePlanner customRoutePlanner = new CustomRoutePlanner(emptyMap, defaultRoutePlanner);
        HttpHost testHost = new HttpHost("someScheme", "wikidata.org", 9999);
        HttpRoute actualRoute = customRoutePlanner.determineRoute(testHost, new BasicHttpContext());
        assertThat(actualRoute.getTargetHost()).isEqualTo(testHost);
    }

    @Test
    void shouldGetCustomRoute() throws HttpException {
        CustomRoutePlanner customRoutePlanner =
                new CustomRoutePlanner(customRouteMapTest, defaultRoutePlanner);
        HttpHost testHost = new HttpHost("someScheme", "managed.test", 9999);
        HttpHost expectedHost = new HttpHost("someScheme", "route.test", 9999);
        HttpRoute actualRoute = customRoutePlanner.determineRoute(testHost, new BasicHttpContext());
        assertThat(actualRoute.getTargetHost()).isEqualTo(expectedHost);
    }

    @Test
    void shouldUseProxyRoute() throws HttpException {
        CustomRoutePlanner customRoutePlanner =
                new CustomRoutePlanner(customRouteMapTest, defaultRoutePlanner);
        HttpHost testHost = new HttpHost("someScheme", "proxy.test", 9999);
        HttpHost expectedHost = new HttpHost("diffScheme", "route.test", 8080);
        HttpRoute actualRoute = customRoutePlanner.determineRoute(testHost, new BasicHttpContext());
        assertThat(actualRoute.getTargetHost()).isEqualTo(expectedHost);
    }
}
