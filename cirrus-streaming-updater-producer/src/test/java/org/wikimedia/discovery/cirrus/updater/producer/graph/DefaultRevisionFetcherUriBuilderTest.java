package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatException;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

class DefaultRevisionFetcherUriBuilderTest {

    private final DefaultRevisionFetcherUriBuilder builder = new DefaultRevisionFetcherUriBuilder();

    @Test
    void revId() {
        InputEvent event = new InputEvent();
        event.setChangeType(ChangeType.REV_BASED_UPDATE);
        event.setRevId(42L);
        event.setTargetDocument(new InputEvent.TargetDocument("local.wikimedia", "junitwiki", 0L, 1L));
        final URI uri = builder.build(event);
        assertThat(uri.getScheme()).isEqualTo("https");
        assertThat(uri.getHost()).isEqualTo("local.wikimedia");
        assertThat(uri.getQuery()).contains("revids=42");
    }

    @Test
    void fails() {
        InputEvent event = new InputEvent();
        event.setChangeType(ChangeType.REV_BASED_UPDATE);
        event.setTargetDocument(
                new InputEvent.TargetDocument("local.wikimedia", "junitwiki", 0L, null));
        assertThatException()
                .isThrownBy(() -> builder.build(event))
                .isInstanceOf(IllegalArgumentException.class);
    }
}
