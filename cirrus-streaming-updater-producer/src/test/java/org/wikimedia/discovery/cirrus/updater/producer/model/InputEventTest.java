package org.wikimedia.discovery.cirrus.updater.producer.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;

import org.apache.commons.io.IOUtils;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.common.typeutils.TypeSerializerSnapshot;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.api.java.typeutils.TypeExtractor;
import org.apache.flink.api.java.typeutils.runtime.TestDataOutputSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.TargetDocument;

/**
 * Test the behavior of the flink Pojo serializer, since we don't have much experience with add
 * couple assertions to make sure it works as we expect (better safe than sorry).
 */
class InputEventTest {
    @Test
    void test_is_flink_pojo() {
        assertThat(TypeExtractor.createTypeInfo(InputEvent.class)).isInstanceOf(PojoTypeInfo.class);
    }

    @Test
    void test_v1_can_be_deserialized() throws IOException {
        assertThat(deserialize(readResource("InputEvent.v1.bytes"))).isEqualTo(v1());
    }

    @Test
    void test_is_current() throws IOException {
        // This test guarantees that whenever we change the shape of InputEvent we make sure to re-run
        // the main method here.
        TypeSerializerSnapshot<Object> snapshot =
                deserializeSnapshotConfig(readResource("InputEvent.current.snapshot.bytes"));
        TypeSerializerSnapshot<InputEvent> actualCurrent =
                TypeExtractor.createTypeInfo(InputEvent.class)
                        .createSerializer(new ExecutionConfig())
                        .snapshotConfiguration();
        assertThat(snapshot.restoreSerializer())
                .withFailMessage(
                        "The shape of InputEvent might have changed please update the test resources.")
                .isEqualTo(actualCurrent.restoreSerializer());
    }

    private byte[] readResource(String name) throws IOException {
        return IOUtils.toByteArray(
                getClass().getResourceAsStream(getClass().getSimpleName() + "/" + name));
    }

    static InputEvent v1() {
        // Do not change this method
        InputEvent e = new InputEvent();
        e.setRequestId("85e66cc8-0d5c-4bcd-87aa-37c5c818314d");
        e.setRevId(123L);
        e.setChangeType(ChangeType.REV_BASED_UPDATE);
        e.setIngestionTime(Instant.parse("2022-11-18T16:53:23Z"));
        e.setEventTime(Instant.parse("2022-11-18T16:52:02Z"));
        e.setEventStream("my_stream");
        final TargetDocument targetDocument =
                new TargetDocument("my.domain.local", "my_database", 2L, 123L);
        targetDocument.setPageTitle("my page title");
        e.setTargetDocument(targetDocument);
        return e;
    }

    static TypeSerializerSnapshot<Object> deserializeSnapshotConfig(byte[] bytes) throws IOException {
        DataInputDeserializer deser = new DataInputDeserializer(bytes);
        return TypeSerializerSnapshot.readVersionedSnapshot(deser, InputEvent.class.getClassLoader());
    }

    static byte[] serializeCurrentSnapshot() throws IOException {
        TypeInformation<InputEvent> typeInfo = TypeExtractor.createTypeInfo(InputEvent.class);
        TestDataOutputSerializer ser = new TestDataOutputSerializer(4096);
        TypeSerializer<InputEvent> typeser = typeInfo.createSerializer(new ExecutionConfig());

        TypeSerializerSnapshot.writeVersionedSnapshot(ser, typeser.snapshotConfiguration());
        return ser.copyByteBuffer();
    }

    static InputEvent deserialize(byte[] bytes) throws IOException {
        DataInputDeserializer deser = new DataInputDeserializer(bytes);
        TypeSerializerSnapshot<Object> objectTypeSerializerSnapshot =
                TypeSerializerSnapshot.readVersionedSnapshot(deser, InputEvent.class.getClassLoader());
        return (InputEvent) objectTypeSerializerSnapshot.restoreSerializer().deserialize(deser);
    }

    static byte[] serialize(InputEvent event) throws IOException {
        TypeInformation<InputEvent> typeInfo = TypeExtractor.createTypeInfo(InputEvent.class);
        TestDataOutputSerializer ser = new TestDataOutputSerializer(4096);
        TypeSerializer<InputEvent> typeSerializer = typeInfo.createSerializer(new ExecutionConfig());

        TypeSerializerSnapshot.writeVersionedSnapshot(ser, typeSerializer.snapshotConfiguration());
        typeSerializer.serialize(event, ser);
        return ser.copyByteBuffer();
    }

    public static void main(String[] args) throws IOException {
        // Generates two payloads:
        // - InputEvent.vX.bytes that contains the current snapshot config (pojo serialization data)
        // - InputEvent.current.snapshot.bytes the current snapshot config
        //
        // When InputEvent changes we make sure that it's backward compatible by
        // - a new vX() method that generates an InputEvent in version X
        // - a new test_vX_can_be_deserialized() by reading the generated file above
        // - InputEvent.current.snapshot.bytes is updated to ensure that test_is_current passes
        final String directory = System.getProperty("java.io.tmpdir");
        Files.write(Paths.get(directory, "InputEvent.v1.bytes"), serialize(v1()));
        Files.write(
                Paths.get(directory, "InputEvent.current.snapshot.bytes"), serializeCurrentSnapshot());

        LoggerFactory.getLogger(InputEventTest.class).info("Wrote to " + directory);
    }
}
