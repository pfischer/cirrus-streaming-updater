package org.wikimedia.discovery.cirrus.updater.producer.sink;

import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.Nonnull;

import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.java.typeutils.runtime.TestDataOutputSerializer;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields.NoopSetParameters;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.Update;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

class UpdateRowEncoderTest {

    final Instant now = Instant.now();
    private final EventRowTypeInfo outputTypeInfo = EventDataStreamUtilities.buildUpdateTypeInfo();
    private final UpdateRowEncoder encoder = new UpdateRowEncoder(outputTypeInfo);

    @Test
    void encodeAndSerialize() throws IOException {
        final Instant now = Instant.now();

        final InputEvent inputEvent = createInputEvent(now);
        inputEvent.getTargetDocument().completeWith("omega", "testwiki_general");
        inputEvent.setUpdate(
                InputEvent.Update.builder().rawFields("{}".getBytes(StandardCharsets.UTF_8)).build());
        final Row row = encoder.encodeInputEvent(inputEvent);

        assertThat(row.getField("dt")).isEqualTo(now);
        assertThat(row.getField("page_id")).isEqualTo(147498L);
        assertThat(row.getField("rev_id")).isEqualTo(551141L);
        assertThat(row.getField("domain")).isEqualTo("my.domain.local");
        assertThat(row.getField("wiki_id")).isEqualTo("mywiki_id");
        assertThat(row.getField("index_name")).isEqualTo("testwiki_general");
        assertThat(row.getField("cluster_group")).isEqualTo("omega");

        final TypeSerializer<Row> rowSerializer = outputTypeInfo.createSerializer(null);
        final TestDataOutputSerializer target = new TestDataOutputSerializer(4096);
        rowSerializer.serialize(row, target);

        assertThat(target.length()).isPositive();
    }

    @Test
    void delete() throws IOException {
        final InputEvent inputEvent = createInputEvent(now);
        inputEvent.setChangeType(ChangeType.PAGE_DELETE);
        inputEvent.getTargetDocument().completeWith("clusterGroup", "indexName");

        assertThat(encoder.encodeInputEvent(inputEvent))
                .extracting(UpdateFields::getOperation)
                .isEqualTo(UpdateFields.OPERATION_DELETE);
    }

    @Test
    void revBasedUpdate() throws IOException {
        final InputEvent inputEvent = createInputEvent(now);
        inputEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        inputEvent.getTargetDocument().completeWith("clusterGroup", "indexName");
        inputEvent.setUpdate(
                InputEvent.Update.builder().rawFields("{}".getBytes(StandardCharsets.UTF_8)).build());

        assertThat(encoder.encodeInputEvent(inputEvent))
                .extracting(UpdateFields::getOperation)
                .isEqualTo(UpdateFields.OPERATION_UPDATE_REVISION);
    }

    @Test
    void redirectAdd() throws IOException {
        final InputEvent inputEvent = createInputEvent(now);
        final TargetDocument originalRedirect = new TargetDocument("domain", "wikiId", 0L, 1L);
        originalRedirect.setPageTitle("A_Redirect");
        inputEvent.setChangeType(ChangeType.REDIRECT_UPDATE);
        inputEvent.getTargetDocument().completeWith("clusterGroup", "indexName");
        inputEvent.setUpdate(Update.forRedirect(newHashSet(originalRedirect), newHashSet()));

        final NoopSetParameters<Map<String, Object>> redirects =
                UpdateFields.getRedirectNoopSetParameters(encoder.encodeInputEvent(inputEvent))
                        .orElseThrow(() -> new NoSuchElementException("Missing redirect noop parameters"));
        assertThat(redirects.getRemove()).isNull();
        assertThat(redirects.getAdd())
                .allSatisfy(
                        encodedRedirect -> {
                            assertThat(encodedRedirect)
                                    .containsEntry(
                                            UpdateFields.REDIRECT_NAMESPACE, originalRedirect.getPageNamespace());
                            assertThat(encodedRedirect)
                                    .containsEntry(UpdateFields.REDIRECT_TITLE, originalRedirect.getPageTitle());
                        });
    }

    @Test
    void redirectRemove() throws IOException {
        final InputEvent inputEvent = createInputEvent(now);
        final TargetDocument originalRedirect = new TargetDocument("domain", "wikiId", 0L, 1L);
        originalRedirect.setPageTitle("A_Redirect");
        inputEvent.setChangeType(ChangeType.REDIRECT_UPDATE);
        inputEvent.getTargetDocument().completeWith("clusterGroup", "indexName");
        inputEvent.setUpdate(Update.forRedirect(newHashSet(), newHashSet(originalRedirect)));

        final NoopSetParameters<Map<String, Object>> redirects =
                UpdateFields.getRedirectNoopSetParameters(encoder.encodeInputEvent(inputEvent))
                        .orElseThrow(() -> new NoSuchElementException("Missing redirect noop parameters"));
        assertThat(redirects.getAdd()).isNull();
        assertThat(redirects.getRemove())
                .allSatisfy(
                        encodedRedirect -> {
                            assertThat(encodedRedirect)
                                    .containsEntry(
                                            UpdateFields.REDIRECT_NAMESPACE, originalRedirect.getPageNamespace());
                            assertThat(encodedRedirect)
                                    .containsEntry(UpdateFields.REDIRECT_TITLE, originalRedirect.getPageTitle());
                        });
    }

    @Nonnull
    private static InputEvent createInputEvent(Instant now) {
        final InputEvent inputEvent = new InputEvent();
        inputEvent.setEventTime(now);
        inputEvent.setTargetDocument(
                new InputEvent.TargetDocument("my.domain.local", "mywiki_id", 0L, 147498L));
        inputEvent.setRevId(551141L);
        inputEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        return inputEvent;
    }
}
