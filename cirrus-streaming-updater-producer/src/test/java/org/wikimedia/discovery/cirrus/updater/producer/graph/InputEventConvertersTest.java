package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.annotation.Nonnull;

import org.apache.commons.io.IOUtils;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.MethodOrderer.MethodName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

@TestMethodOrder(MethodName.class)
class InputEventConvertersTest {
    private final RevisionFetcher.EventAugmenter augmenter =
            InputEventConverters.cirrusDocAugmenter(new ObjectMapper());
    private final EventDataStreamFactory factory =
            EventDataStreamFactory.from(
                    singletonList(InputEventConvertersTest.class.getResource("/schema_repo").toString()),
                    this.getClass().getResource("/event-stream-config.json").toString());

    // use cases:
    // 00) create A; -> [Insert A]
    // 01) update A; -> [Upsert A]
    // 02) create B (redirect to A); -> [Delete B, Upsert A]
    // 03) move A to A_Moved; -> [Insert A_Moved, Delete A]
    // 04) update B (redirect to A_Moved) -> [Upsert A_Moved]
    // 05) delete B -> [Upsert A_Moved]

    @Test
    void pageChange00CreateA() throws IOException {
        final List<InputEvent> events = mapPageChangeEvents("00.create_a");
        assertThat(events).hasSize(1);
        final InputEvent inputEvent = events.get(0);
        validateCommonPageChangeAttributes(inputEvent);
        assertThat(inputEvent.getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(inputEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A");
    }

    @Test
    void pageChange01UpdateA() throws IOException {
        final List<InputEvent> events = mapPageChangeEvents("01.update_a");
        assertThat(events).hasSize(1);
        final InputEvent inputEvent = events.get(0);
        validateCommonPageChangeAttributes(inputEvent);
        assertThat(inputEvent.getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(inputEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A");
    }

    @Test
    void pageChange02CreateB() throws IOException {
        final List<InputEvent> events = mapPageChangeEvents("02.create_b");
        assertThat(events).hasSize(2);

        final InputEvent deleteEvent = events.get(0);
        validateCommonPageChangeAttributes(deleteEvent);
        assertThat(deleteEvent.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(deleteEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_B");

        final InputEvent linksEvent = events.get(1);
        assertThat(linksEvent.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(linksEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A");
    }

    @Test
    void pageChange03MoveA() throws IOException {
        final List<InputEvent> events = mapPageChangeEvents("03.move_a");
        assertThat(events).hasSize(1);

        final InputEvent deleteEvent = events.get(0);
        validateCommonPageChangeAttributes(deleteEvent);
        assertThat(deleteEvent.getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(deleteEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A_Moved");
    }

    @Test
    void pageChange03UpdateA() throws IOException {
        final List<InputEvent> events = mapPageChangeEvents("03.update_a");
        assertThat(events).hasSize(2);

        final InputEvent deleteEvent = events.get(0);
        validateCommonPageChangeAttributes(deleteEvent);
        assertThat(deleteEvent.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(deleteEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A");

        final InputEvent linksEvent = events.get(1);
        assertThat(linksEvent.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(linksEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A_Moved");
    }

    @Test
    void pageChange04UpdateB() throws IOException {
        final List<InputEvent> events = mapPageChangeEvents("04.update_b");
        assertThat(events).hasSize(2);

        final InputEvent deleteEvent = events.get(0);
        validateCommonPageChangeAttributes(deleteEvent);
        assertThat(deleteEvent.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(deleteEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_B");

        final InputEvent linksEvent = events.get(1);
        assertThat(linksEvent.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(linksEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A_Moved");
    }

    @Test
    void pageChange05DeleteB() throws IOException {
        final List<InputEvent> events = mapPageChangeEvents("05.delete_b");
        assertThat(events).hasSize(2);

        final InputEvent deleteEvent = events.get(0);
        validateCommonPageChangeAttributes(deleteEvent);
        assertThat(deleteEvent.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(deleteEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_B");

        final InputEvent linksEvent = events.get(1);
        assertThat(linksEvent.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(linksEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A_Moved");
    }

    private static void validateCommonPageChangeAttributes(InputEvent inputEvent) {
        InputEvent.TargetDocument target = inputEvent.getTargetDocument();
        assertThat(inputEvent.getEventTime()).isNotNull();
        assertThat(inputEvent.getEventStream()).isEqualTo("mediawiki.page_change");
        assertThat(target.getWikiId()).isEqualTo("my_wiki");
        assertThat(inputEvent.getRequestId()).isNotEmpty();
        assertThat(target.getPageNamespace()).isEqualTo(0);
        assertThat(target.getPageId()).isGreaterThan(0);
        assertThat(inputEvent.getRevId()).isGreaterThan(0);
        assertThat(inputEvent.getIngestionTime()).isNull();
    }

    private List<InputEvent> mapPageChangeEvents(String sourceEventFileName) throws IOException {
        final Row row = createEventRow(sourceEventFileName);
        return StreamSupport.stream(InputEventConverters.fromPageChange(row).spliterator(), false)
                .collect(Collectors.toList());
    }

    private Row createEventRow(String sourceEventFileName) throws IOException {
        byte[] eventData =
                IOUtils.toByteArray(
                        getClass()
                                .getResourceAsStream(
                                        getClass().getSimpleName()
                                                + "/page_change-event."
                                                + sourceEventFileName
                                                + ".json"));
        return factory.deserializer("rc0.mediawiki.page_change").deserialize(eventData);
    }

    @Test
    void happyPath() throws IOException {
        final Instant now = Instant.now();
        final InputEvent input = createInputEvent(now);
        final InputEvent output =
                augmenter.augmentWithCirrusBuildDoc(
                        input,
                        EventDataStreamUtilities.parseJson("/wiremock/__files/revision.551141.json")
                                .at("/query/pages/0"));

        InputEvent.TargetDocument target = output.getTargetDocument();
        assertThat(output.getEventTime()).isEqualTo(now);
        assertThat(output.getRevId()).isEqualTo(551141L);
        assertThat(target.getPageId()).isEqualTo(147498L);
        assertThat(target.getDomain()).isEqualTo("my.domain.local");
        assertThat(target.getWikiId()).isEqualTo("mywiki_id");
        assertThat(target.getIndexName()).isEqualTo("testwiki_general");
        assertThat(target.getClusterGroup()).isEqualTo("omega");
        assertThat(target.isComplete()).isTrue();
    }

    @Test
    void augmentFailsIfInputIsIncomplete() {
        assertThatThrownBy(
                        () -> {
                            augmenter.augmentWithCirrusBuildDoc(
                                    createInputEvent(Instant.now()),
                                    EventDataStreamUtilities.parseJson(
                                            "/wiremock/__files/revision.551141.no-metadata.json"));
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void augmentFailsIfPageIdDoesNotMatch() {
        final InputEvent inputEvent = createInputEvent(Instant.now());
        InputEvent.TargetDocument old = inputEvent.getTargetDocument();
        inputEvent.setTargetDocument(
                new InputEvent.TargetDocument(
                        old.getDomain(), old.getWikiId(), old.getPageNamespace(), -1L));

        assertThatThrownBy(
                        () -> {
                            augmenter.augmentWithCirrusBuildDoc(
                                    inputEvent,
                                    EventDataStreamUtilities.parseJson("/wiremock/__files/revision.551141.json")
                                            .at("/query/pages/0"));
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void augmentFailsIfRevIdDoesNotMatch() {
        final InputEvent inputEvent = createInputEvent(Instant.now());
        inputEvent.setRevId(-1L);

        assertThatThrownBy(
                        () -> {
                            augmenter.augmentWithCirrusBuildDoc(
                                    inputEvent,
                                    EventDataStreamUtilities.parseJson("/wiremock/__files/revision.551141.json")
                                            .at("/query/pages/0"));
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Nonnull
    private static InputEvent createInputEvent(Instant now) {
        final InputEvent inputEvent = new InputEvent();
        inputEvent.setEventTime(now);
        inputEvent.setRevId(551141L);
        inputEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        inputEvent.setTargetDocument(
                new InputEvent.TargetDocument("my.domain.local", "mywiki_id", 0L, 147498L));
        return inputEvent;
    }
}
