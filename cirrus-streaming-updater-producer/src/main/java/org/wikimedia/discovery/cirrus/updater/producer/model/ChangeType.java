package org.wikimedia.discovery.cirrus.updater.producer.model;

public enum ChangeType {
    REV_BASED_UPDATE,
    PAGE_DELETE,
    TAGS_UPDATE,
    REDIRECT_UPDATE,
}
