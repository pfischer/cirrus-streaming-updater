package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static com.google.common.base.Preconditions.checkArgument;
import static org.wikimedia.discovery.cirrus.updater.producer.model.JsonPathUtils.getRequiredNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.TargetDocument;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.collect.ImmutableSet;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
@SuppressFBWarnings(value = "CE_CLASS_ENVY", justification = "false positive")
public class InputEventConverters {

    private static final String PAGE_CHANGE_SCHEMA = "/mediawiki/page/change";
    private static final String REVISION_SCORE_SCHEMA = "/mediawiki/revision/score";
    private static final String RECOMMENDATION_CREATE_SCHEMA =
            "/mediawiki/revision/recommendation-create";

    private static void checkSchema(Row row, String expectedSchema) {
        final String schema = row.getFieldAs("$schema");
        checkArgument(schema != null, "Undefined source schema");
        checkArgument(schema.startsWith(expectedSchema), "Unknown source schema: " + schema);
    }

    private static void applyEventMeta(Row row, InputEvent event) {
        Row meta = row.getFieldAs("meta");
        event.setRequestId(meta.getFieldAs("request_id"));
        event.setEventStream(meta.getFieldAs("stream"));
        event.setEventTime(meta.getFieldAs("dt"));
    }

    /**
     * Maps a <a
     * href="https://schema.wikimedia.org/repositories/primary/jsonschema/mediawiki/page/change/latest">mediawiki/page/change</a>
     * event to {@code 0..n} derived {@link InputEvent InputEvents}.
     *
     * <p>Depending on the {@code page_change_kind}…
     *
     * <ul>
     *   <li>…<code>delete</code> results in…
     *       <ul>
     *         <li>…a delete operation
     *         <li>…an update operation of the redirect target page (if any)
     *       </ul>
     *   <li>…<code>create, edit, move, …</code> result in…
     *       <ul>
     *         <li>…a revision-based update if the source page <i>is not</i> a redirect
     *         <li>…a delete operation if the source page <i>is</i> a redirect
     *         <li>…an update operation of the redirect target page (if any)
     *       </ul>
     * </ul>
     *
     * @param row the original event
     * @return {@code 0..n} derived {@link InputEvent InputEvents}
     */
    public static Iterable<InputEvent> fromPageChange(Row row) {
        checkSchema(row, PAGE_CHANGE_SCHEMA);

        List<InputEvent> events = new ArrayList<>(2);
        InputEvent sourceEvent = new InputEvent();
        events.add(sourceEvent);

        applyEventMeta(row, sourceEvent);

        String pageChangeKind = row.getFieldAs("page_change_kind");

        Row meta = row.getFieldAs("meta");
        Row page = row.getFieldAs("page");

        Row revision = row.getFieldAs("revision");
        sourceEvent.setRevId(revision.getFieldAs("rev_id"));
        sourceEvent.setEventTime(revision.getFieldAs("rev_dt"));

        final TargetDocument targetDocument =
                new TargetDocument(
                        meta.getFieldAs("domain"), row.getFieldAs("wiki_id"),
                        page.getFieldAs("namespace_id"), page.getFieldAs("page_id"));
        targetDocument.setPageTitle(page.getFieldAs("page_title"));
        sourceEvent.setTargetDocument(targetDocument);

        if ("delete".equals(pageChangeKind)) {
            sourceEvent.setChangeType(ChangeType.PAGE_DELETE);
        } else {
            sourceEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        }

        deriveRedirectTargetInputEvent(row)
                .ifPresent(
                        targetEvent -> {
                            final ImmutableSet<TargetDocument> redirect =
                                    ImmutableSet.of(sourceEvent.getTargetDocument());
                            if (sourceEvent.getChangeType() == ChangeType.PAGE_DELETE) {
                                targetEvent.setUpdate(InputEvent.Update.builder().redirectRemove(redirect).build());
                            } else {
                                targetEvent.setUpdate(InputEvent.Update.builder().redirectAdd(redirect).build());
                            }

                            events.add(targetEvent);

                            sourceEvent.setChangeType(ChangeType.PAGE_DELETE);
                        });

        return events;
    }

    /**
     * Maps an optional redirect target as {@link InputEvent}.
     *
     * @param event the source {@code page_change} event
     * @return An input event, if {@code /page/redirect_page_link} is not null, and is not a redirect
     *     itself
     */
    private static Optional<InputEvent> deriveRedirectTargetInputEvent(Row event) {
        return Optional.ofNullable(event.<Row>getFieldAs("page"))
                .map(page -> page.<Row>getFieldAs("redirect_page_link"))
                .filter(
                        link ->
                                !Optional.ofNullable(link.<Boolean>getFieldAs("is_redirect")).orElse(Boolean.FALSE))
                .map(
                        link -> {
                            final Row meta = event.getFieldAs("meta");
                            InputEvent targetEvent = new InputEvent();
                            targetEvent.setChangeType(ChangeType.REDIRECT_UPDATE);
                            applyEventMeta(event, targetEvent);

                            final TargetDocument targetDocument =
                                    new TargetDocument(
                                            meta.getFieldAs("domain"),
                                            event.getFieldAs("wiki_id"),
                                            link.getFieldAs("namespace_id"),
                                            link.getFieldAs("page_id"));
                            targetDocument.setPageTitle(link.getFieldAs("page_title"));
                            targetEvent.setTargetDocument(targetDocument);
                            return targetEvent;
                        });
    }

    private static final int SCALE_FACTOR = 1000;
    private static final String TOPIC_SEPARATOR = "/";
    private static final String PROBABILITY_SEPARATOR = "|";

    private static String stringifyPrediction(String prefix, String topic, float prob) {
        return stringifyPrediction(prefix, topic, (int) (prob * SCALE_FACTOR));
    }

    private static String stringifyPrediction(String prefix, String topic, int prob) {
        checkArgument(!topic.contains(PROBABILITY_SEPARATOR), "Topic names must not contain |");
        return prefix + TOPIC_SEPARATOR + topic + PROBABILITY_SEPARATOR + prob;
    }

    private static List<String> preprocessRevisionScoring(Row row, String prefix) {
        Row scores = row.getFieldAs("scores");
        Set<String> modelNames = scores.getFieldNames(false);
        return modelNames.stream()
                .map(scores::<Row>getFieldAs)
                .flatMap(
                        score -> {
                            List<String> topics = score.getFieldAs("prediction");
                            Map<String, Float> probability = score.getFieldAs("probability");
                            return topics.stream()
                                    .map(topic -> stringifyPrediction(prefix, topic, probability.get(topic)));
                        })
                .collect(Collectors.toList());
    }

    public static InputEvent fromRevisionScoring(Row row, String predictionPrefix) {
        checkSchema(row, REVISION_SCORE_SCHEMA);
        InputEvent event = new InputEvent();
        applyEventMeta(row, event);

        Row meta = row.getFieldAs("meta");
        event.setChangeType(ChangeType.TAGS_UPDATE);
        final TargetDocument targetDocument =
                new TargetDocument(
                        row.getFieldAs("database"), meta.getFieldAs("domain"),
                        row.getFieldAs("page_namespace"), row.getFieldAs("page_id"));
        targetDocument.setPageTitle(row.getFieldAs("page_title"));
        event.setTargetDocument(targetDocument);
        event.setRevId(row.getFieldAs("rev_id"));
        event.setUpdate(
                InputEvent.Update.forWeightedTags(preprocessRevisionScoring(row, predictionPrefix)));

        return event;
    }

    private static List<String> preprocessRecommendationCreate(Row row) {
        String recommendationType = row.getFieldAs("recommendationType");
        String prefix = "recommendation." + recommendationType;
        // The only information to share about recommendations is if they exist, provided a constant
        // topic and probability to match the use case.
        return Collections.singletonList(stringifyPrediction(prefix, "exists", 1));
    }

    public static InputEvent fromRecommendationCreate(Row row) {
        checkSchema(row, RECOMMENDATION_CREATE_SCHEMA);
        InputEvent event = new InputEvent();
        applyEventMeta(row, event);

        event.setChangeType(ChangeType.TAGS_UPDATE);
        Row meta = row.getFieldAs("meta");
        final TargetDocument targetDocument =
                new TargetDocument(
                        row.getFieldAs("database"), meta.getFieldAs("domain"),
                        row.getFieldAs("page_namespace"), row.getFieldAs("page_id"));
        targetDocument.setPageTitle(row.getFieldAs("page_title"));
        event.setTargetDocument(targetDocument);
        event.setRevId(row.getFieldAs("rev_id"));
        event.setUpdate(InputEvent.Update.forWeightedTags(preprocessRecommendationCreate(row)));

        return event;
    }

    /** Augments InputEvent with the cirrusdoc response from CirrusSearch API. */
    public static RevisionFetcher.EventAugmenter cirrusDocAugmenter(ObjectMapper objectMapper) {
        ObjectWriter objectWriter = objectMapper.writer();
        return (InputEvent inputEvent, JsonNode page) -> {
            final JsonNode cirrusBuildDoc = getRequiredNode(page, "/cirrusbuilddoc");
            final JsonNode cirrusBuildDocMetadata = getRequiredNode(page, "/cirrusbuilddoc_metadata");

            Long pageId = getRequiredNode(cirrusBuildDoc, "/page_id").asLong();
            if (!pageId.equals(inputEvent.getTargetDocument().getPageId())) {
                throw new IllegalArgumentException("Received response for wrong page id");
            }
            Long revId = getRequiredNode(cirrusBuildDoc, "/version").asLong();
            if (inputEvent.getRevId() == null) {
                // TODO: Does this do anything? I don't think we use the rev id after fetching
                inputEvent.setRevId(revId);
            } else if (!inputEvent.getRevId().equals(revId)) {
                throw new IllegalArgumentException("Received response for wrong revision id");
            }

            InputEvent.TargetDocument target = inputEvent.getTargetDocument();
            Long namespaceId = getRequiredNode(cirrusBuildDoc, "/namespace").asLong();
            if (!target.getPageNamespace().equals(namespaceId)) {
                // How could namespace disagree? Perhaps the page has moved since the initial event
                // was fired. This api response should be the most recent information, lets set it
                target =
                        new InputEvent.TargetDocument(
                                target.getDomain(), target.getWikiId(), namespaceId, pageId);
                inputEvent.setTargetDocument(target);
            }

            String clusterGroup = getRequiredNode(cirrusBuildDocMetadata, "/cluster_group").asText();
            String indexName = getRequiredNode(cirrusBuildDocMetadata, "/index_name").asText();
            target.completeWith(clusterGroup, indexName);

            final Map<String, String> noopHints =
                    objectMapper.convertValue(
                            getRequiredNode(cirrusBuildDocMetadata, "/noop_hints"),
                            new TypeReference<Map<String, String>>() {});

            final byte[] rawFields = objectWriter.writeValueAsBytes(cirrusBuildDoc);
            inputEvent.setUpdate(
                    InputEvent.Update.builder().noopHints(noopHints).rawFields(rawFields).build());
            return inputEvent;
        };
    }
}
