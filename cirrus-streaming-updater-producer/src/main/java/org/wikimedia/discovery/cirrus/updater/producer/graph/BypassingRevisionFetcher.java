package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

import org.wikimedia.discovery.cirrus.updater.common.async.CompletableFutureBackPorts;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

/**
 * Wrapper around {@link RevisionFetcher} that bypasses fetching revisions under certain conditions.
 *
 * <p>Allows skipping {@link ChangeType#PAGE_DELETE}s while maintaining the order of events from
 * flink's perspective.
 */
public class BypassingRevisionFetcher
        implements Function<InputEvent, CompletionStage<InputEvent>>, Serializable, Closeable {

    private static final long serialVersionUID = 5835331764827371291L;
    private final RevisionFetcher delegate;

    public BypassingRevisionFetcher(RevisionFetcher delegate) {
        this.delegate = delegate;
    }

    @Override
    public CompletionStage<InputEvent> apply(InputEvent inputEvent) {
        final ChangeType changeType = inputEvent.getChangeType();
        return changeType == ChangeType.PAGE_DELETE || changeType == ChangeType.REDIRECT_UPDATE
                ? CompletableFutureBackPorts.completedFuture(inputEvent)
                : delegate.apply(inputEvent);
    }

    @Override
    public void close() throws IOException {
        delegate.close();
    }
}
