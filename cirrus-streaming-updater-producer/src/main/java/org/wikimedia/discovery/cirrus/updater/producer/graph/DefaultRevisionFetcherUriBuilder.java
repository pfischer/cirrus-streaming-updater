package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.net.URI;

import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.TargetDocument;

public class DefaultRevisionFetcherUriBuilder implements RevisionFetcherUriBuilder {

    private static final long serialVersionUID = 5338550049892592030L;
    protected static final String REV_IDS_QUERY_SUFFIX = "&revids=";
    protected static final String PAGE_IDS_QUERY_SUFFIX = "&pageids=";
    private static final String MW_API_PATH_AND_QUERY =
            "/w/api.php?action=query&format=json&cbbuilders=content&prop=cirrusbuilddoc&formatversion=2&format=json";

    @Override
    public URI build(InputEvent inputEvent) {
        final TargetDocument targetDocument = inputEvent.getTargetDocument();
        final StringBuilder builder =
                new StringBuilder("https://")
                        .append(targetDocument.getDomain())
                        .append(MW_API_PATH_AND_QUERY);

        if (inputEvent.getRevId() != null) {
            builder.append(REV_IDS_QUERY_SUFFIX).append(inputEvent.getRevId());
        } else {
            throw new IllegalArgumentException("Revision ID must be set");
        }

        return URI.create(builder.toString());
    }
}
