package org.wikimedia.discovery.cirrus.updater.producer.model;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
public class InputEvent {
    ChangeType changeType;
    String requestId;
    String eventStream;
    Instant eventTime;
    Long revId;
    Instant ingestionTime;
    TargetDocument targetDocument;
    Update update;

    /** Targeting information for a single document in elasticsearch. */
    @Data
    @RequiredArgsConstructor
    public static class TargetDocument {
        private final String domain;
        private final String wikiId;
        private final Long pageNamespace;
        private final Long pageId;
        private String clusterGroup;
        private String indexName;

        private String pageTitle;

        public boolean isComplete() {
            return clusterGroup != null && indexName != null;
        }

        public void completeWith(String clusterGroup, String indexName) {
            this.clusterGroup = clusterGroup;
            this.indexName = indexName;
        }
    }

    @Data
    @AllArgsConstructor
    @Builder(toBuilder = true)
    public static class Update {

        public static final Map<String, String> WEIGHTED_TAGS_HINT =
                Collections.singletonMap(UpdateFields.WEIGHTED_TAGS, "multilist");

        public static final Map<String, String> REDIRECT_HINT =
                Collections.singletonMap(UpdateFields.REDIRECT, "set");

        private final Map<String, String> noopHints;

        private final byte[] rawFields;
        private final Set<String> weightedTags;

        private final Set<TargetDocument> redirectRemove;
        private final Set<TargetDocument> redirectAdd;

        public static Update forWeightedTags(Collection<String> weightedTags) {
            return builder()
                    .noopHints(WEIGHTED_TAGS_HINT)
                    .weightedTags(weightedTags == null || weightedTags.isEmpty() ? null : asSet(weightedTags))
                    .build();
        }

        public static Update forRedirect(Set<TargetDocument> add, Set<TargetDocument> remove) {
            return builder()
                    .noopHints(REDIRECT_HINT)
                    .redirectAdd(add == null || add.isEmpty() ? null : add)
                    .redirectRemove(remove == null || remove.isEmpty() ? null : remove)
                    .build();
        }

        private static <T> Set<T> asSet(Collection<T> weightedTags) {
            return weightedTags instanceof Set ? (Set<T>) weightedTags : new HashSet<>(weightedTags);
        }
    }
}
