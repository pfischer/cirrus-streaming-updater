package org.wikimedia.discovery.cirrus.updater.producer.config;

import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG;
import static org.wikimedia.discovery.cirrus.updater.common.config.ReadableConfigReader.getRequired;
import static org.wikimedia.discovery.cirrus.updater.common.config.ReadableConfigReader.getRequiredMapEntry;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.configuration.ConfigOptions;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.wikimedia.discovery.cirrus.updater.common.config.ReadableConfigReader;
import org.wikimedia.eventutilities.core.SerializableClock;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@Builder
@Accessors(fluent = true)
@ParametersAreNonnullByDefault
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProducerConfig {

    public static final ConfigOption<Boolean> PARAM_DRY_RUN =
            ConfigOptions.key("dry-run").booleanType().defaultValue(Boolean.FALSE);

    public static final ConfigOption<String> PARAM_PAGE_CHANGE_STREAM =
            ConfigOptions.key("page-change-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_ARTICLE_TOPIC_STREAM =
            ConfigOptions.key("article-topic-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_DRAFT_TOPIC_STREAM =
            ConfigOptions.key("draft-topic-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_RECOMMENDATION_CREATE_STREAM =
            ConfigOptions.key("recommendation-create-stream").stringType().noDefaultValue();

    public static final ConfigOption<Map<String, String>> PARAM_KAFKA_SOURCE_CONFIG =
            ConfigOptions.key("kafka-source-config").mapType().noDefaultValue();

    public static final ConfigOption<Map<String, String>> PARAM_KAFKA_SINK_CONFIG =
            ConfigOptions.key("kafka-sink-config").mapType().noDefaultValue();
    public static final ConfigOption<Duration> PARAM_FETCH_REQUEST_TIMEOUT =
            ConfigOptions.key("fetch-request-timeout")
                    .durationType()
                    .defaultValue(Duration.ofSeconds(5))
                    .withDescription("Timeout per fetch request");

    public static final ConfigOption<Integer> PARAM_RETRY_FETCH_MAX =
            ConfigOptions.key("fetch-retry-max").intType().defaultValue(3);

    public static final ConfigOption<Duration> PARAM_FETCH_RETRY_MAX_AGE =
            ConfigOptions.key("fetch-retry-max-age")
                    .durationType()
                    .defaultValue(Duration.ofSeconds(10))
                    .withDescription(
                            "Max age of the events for which fetching the revision content is retried regardless of "
                                    + PARAM_RETRY_FETCH_MAX.key());
    public static final ConfigOption<Integer> PARAM_FETCH_RETRY_QUEUE_CAPACITY =
            ConfigOptions.key("fetch-retry-queue-capacity").intType().defaultValue(100);
    public static final ConfigOption<String> PARAM_KAFKA_SOURCE_START_TIME =
            ConfigOptions.key("kafka-source-start-time").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_KAFKA_SOURCE_END_TIME =
            ConfigOptions.key("kafka-source-end-time").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_EVENT_STREAM_CONFIG_URL =
            ConfigOptions.key("event-stream-config-url").stringType().noDefaultValue();
    public static final ConfigOption<List<String>> PARAM_EVENT_STREAM_JSON_SCHEMA_URLS =
            ConfigOptions.key("event-stream-json-schema-urls").stringType().asList().noDefaultValue();
    public static final ConfigOption<Map<String, String>> PARAM_HTTP_ROUTES =
            ConfigOptions.key("http-routes")
                    .mapType()
                    .noDefaultValue()
                    .withDescription("A map of host -> URL");
    public static final ConfigOption<String> PARAM_PIPELINE_NAME =
            ConfigOptions.key("pipeline").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_FETCH_FAILURE_TOPIC =
            ConfigOptions.key("fetch-failure-topic").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_FETCH_SUCCESS_TOPIC =
            ConfigOptions.key("fetch-success-topic").stringType().noDefaultValue();
    public static final ConfigOption<Integer> PARAM_WINDOW_SIZE_MINUTES =
            ConfigOptions.key("window-size-minutes").intType().defaultValue(5);

    String eventStreamConfigUrl;
    List<String> eventStreamJsonSchemaUrls;

    String pageChangeStream;
    String articleTopicStream;
    String draftTopicStream;
    String recommendationCreateStream;

    Map<String, String> kafkaSourceConfig;

    String kafkaSourceBootstrapServers;
    String kafkaSourceGroupId;

    @Nullable Instant kafkaSourceStartTime;
    @Nullable Instant kafkaSourceEndTime;

    Map<String, String> kafkaSinkConfig;
    String kafkaSinkBootstrapServers;

    @Nullable Map<String, String> httpRoutes;

    String pipeline;

    String fetchFailureTopic;
    String fetchSuccessTopic;

    @Builder.Default Duration retryFetchMaxAge = PARAM_FETCH_RETRY_MAX_AGE.defaultValue();

    @Builder.Default Duration fetchRequestTimeout = PARAM_FETCH_REQUEST_TIMEOUT.defaultValue();

    @Builder.Default int retryFetchMax = PARAM_RETRY_FETCH_MAX.defaultValue();

    @Builder.Default int retryFetchQueueCapacity = PARAM_FETCH_RETRY_QUEUE_CAPACITY.defaultValue();

    @Builder.Default SerializableClock clock = Instant::now;

    @Builder.Default boolean dryRun = PARAM_DRY_RUN.defaultValue();

    Time windowSize;

    public static ProducerConfig of(ParameterTool params) {
        final Configuration configuration = params.getConfiguration();

        final String kafkaSourceBootstrapServers =
                getRequiredMapEntry(configuration, PARAM_KAFKA_SOURCE_CONFIG, BOOTSTRAP_SERVERS_CONFIG);

        final ProducerConfigBuilder builder =
                ProducerConfig.builder()
                        .eventStreamConfigUrl(getRequired(configuration, PARAM_EVENT_STREAM_CONFIG_URL))
                        .eventStreamJsonSchemaUrls(
                                getRequired(configuration, PARAM_EVENT_STREAM_JSON_SCHEMA_URLS))
                        .pageChangeStream(getRequired(configuration, PARAM_PAGE_CHANGE_STREAM))
                        .articleTopicStream(getRequired(configuration, PARAM_ARTICLE_TOPIC_STREAM))
                        .draftTopicStream(getRequired(configuration, PARAM_DRAFT_TOPIC_STREAM))
                        .recommendationCreateStream(
                                getRequired(configuration, PARAM_RECOMMENDATION_CREATE_STREAM))
                        .kafkaSourceConfig(getRequired(configuration, PARAM_KAFKA_SOURCE_CONFIG))
                        .kafkaSourceBootstrapServers(kafkaSourceBootstrapServers)
                        .kafkaSourceGroupId(
                                getRequiredMapEntry(configuration, PARAM_KAFKA_SOURCE_CONFIG, GROUP_ID_CONFIG))
                        .kafkaSinkConfig(
                                configuration.getOptional(PARAM_KAFKA_SINK_CONFIG).orElse(Collections.emptyMap()))
                        .kafkaSinkBootstrapServers(
                                ReadableConfigReader.getOptionalMapEntry(
                                                configuration, PARAM_KAFKA_SINK_CONFIG, BOOTSTRAP_SERVERS_CONFIG)
                                        .orElse(kafkaSourceBootstrapServers))
                        .fetchFailureTopic(getRequired(configuration, PARAM_FETCH_FAILURE_TOPIC))
                        .fetchSuccessTopic(getRequired(configuration, PARAM_FETCH_SUCCESS_TOPIC))
                        .httpRoutes(configuration.get(PARAM_HTTP_ROUTES))
                        .pipeline(getRequired(configuration, PARAM_PIPELINE_NAME))
                        .dryRun(getRequired(configuration, PARAM_DRY_RUN))
                        .windowSize(Time.minutes(getRequired(configuration, PARAM_WINDOW_SIZE_MINUTES)));

        configuration.getOptional(PARAM_FETCH_RETRY_MAX_AGE).ifPresent(builder::retryFetchMaxAge);
        configuration.getOptional(PARAM_FETCH_REQUEST_TIMEOUT).ifPresent(builder::fetchRequestTimeout);
        configuration.getOptional(PARAM_RETRY_FETCH_MAX).ifPresent(builder::retryFetchMax);
        configuration
                .getOptional(PARAM_FETCH_RETRY_QUEUE_CAPACITY)
                .ifPresent(builder::retryFetchQueueCapacity);
        configuration
                .getOptional(PARAM_KAFKA_SOURCE_START_TIME)
                .map(Instant::parse)
                .ifPresent(builder::kafkaSourceStartTime);
        configuration
                .getOptional(PARAM_KAFKA_SOURCE_END_TIME)
                .map(Instant::parse)
                .ifPresent(builder::kafkaSourceEndTime);

        return builder.build();
    }

    @Nonnull
    public Properties kafkaSourceProperties() {
        return asProperties(kafkaSourceConfig);
    }

    @Nonnull
    public Properties kafkaSinkProperties() {
        return asProperties(kafkaSinkConfig);
    }

    @Nonnull
    private static Properties asProperties(Map<String, String> config) {
        final Properties properties = new Properties();
        properties.putAll(config);
        return properties;
    }
}
