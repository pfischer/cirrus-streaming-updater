package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static java.util.Objects.requireNonNull;
import static org.wikimedia.discovery.cirrus.updater.producer.model.JsonPathUtils.getRequiredNode;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.Map;

import javax.annotation.Nonnull;

import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.wikimedia.discovery.cirrus.updater.producer.http.HttpClientFactory;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressFBWarnings({"SE_NO_SERIALVERSIONID", "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE"})
public class CirrusNamespaceIndexMap extends RichMapFunction<InputEvent, InputEvent> {
    private static final String API_PATH =
            "/w/api.php?action=cirrus-config-dump&prop=replicagroup%7Cnamespacemap&format=json&formatversion=2";
    private static final Duration TTL = Duration.ofHours(6);
    private static final int ATTEMPTS = 5;
    /** tune the delay between retries: 0.1 should give 0.1, 0.3, 0.7 and 1.5 seconds. */
    private static final double BACKOFF_FACTOR = 0.1;

    private static final String DEFAULT_INDEX_TYPE = "general";
    public static final String CLUSTER_GROUP = "cluster_group";
    public static final String INDEX_NAME = "index_name";
    public static final String WIKI_ID = "wiki_id";
    public static final String DOMAIN = "domain";
    public static final String PAGE_NAMESPACE = "page_namespace";
    private transient CloseableHttpClient client;
    private final Map<String, String> httpRoutes;
    private final Duration httpRequestTimeout;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private transient Cache<String, IndexSettings> cache;

    private transient Counter misses;
    private transient Counter total;
    private transient Counter skipped;
    private transient Counter apiCall;
    private transient Counter unknownNamespace;

    public CirrusNamespaceIndexMap(Map<String, String> httpRoutes, Duration httpRequestTimeout) {
        this.httpRoutes = httpRoutes;
        this.httpRequestTimeout = httpRequestTimeout;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        misses = getRuntimeContext().getMetricGroup().counter("namespace-map-info-misses");
        total = getRuntimeContext().getMetricGroup().counter("namespace-map-info-total");
        skipped = getRuntimeContext().getMetricGroup().counter("namespace-map-info-skipped");
        apiCall = getRuntimeContext().getMetricGroup().counter("namespace-map-info-api-calls");
        unknownNamespace =
                getRuntimeContext().getMetricGroup().counter("namespace-map-info-unknown-ns");
        cache = Caffeine.newBuilder().expireAfterWrite(TTL).build();
    }

    @Override
    @SuppressFBWarnings(
            value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE",
            justification = "false positive on cache.get return a null value")
    public InputEvent map(InputEvent inputEvent) {
        total.inc();
        InputEvent.TargetDocument target = inputEvent.getTargetDocument();
        if (target.isComplete()) {
            skipped.inc();
            return inputEvent;
        }
        String wikiId = requireNonNull(target.getWikiId());
        String domain = requireNonNull(target.getDomain());
        Long namespace = requireNonNull(target.getPageNamespace());

        IndexSettings settings = cache.get(wikiId, k -> load(k, domain));
        if (settings == null) {
            log.warn("No (cached) settings for wiki [{}]", wikiId);
            // With an incomplete target, this event will end up being dropped at the output stage.
            return inputEvent;
        }
        String indexName = settings.getNamespaceMap().get(namespace);
        if (indexName == null) {
            unknownNamespace.inc();
            indexName = wikiId + "_" + DEFAULT_INDEX_TYPE;
            log.warn("Unknown namespace [{}] for wiki [{}], using [{}]", namespace, wikiId, indexName);
        }
        target.completeWith(settings.getCirrusReplica(), indexName);
        return inputEvent;
    }

    @SneakyThrows(InterruptedException.class)
    @Nonnull
    private IndexSettings load(String wikiId, String domain) {
        misses.inc();
        Throwable thrown = null;
        for (int attempt = 0; attempt < ATTEMPTS; attempt++) {
            try {
                long sleep = (long) backoffDelayMs(attempt);
                if (attempt > 0) {
                    Thread.sleep(sleep);
                }
                return fetch(domain, wikiId);
            } catch (IOException | InvalidMWApiResponseException e) {
                thrown = e;
            }
        }
        throw new IllegalStateException("Failed to extract namespacemap for [" + wikiId + "]", thrown);
    }

    private double backoffDelayMs(int attempt) {
        return 1000D * (BACKOFF_FACTOR * (Math.pow(2D, attempt) - 1D));
    }

    private IndexSettings fetch(String domain, String wikiId) throws IOException {
        apiCall.inc();
        try (CloseableHttpResponse response = getClient().execute(new HttpGet(buildUri(domain)))) {
            if (response.getCode() != 200) {
                throw new InvalidMWApiResponseException(response.getReasonPhrase());
            }
            try (JsonParser parser = objectMapper.createParser(response.getEntity().getContent())) {
                JsonNode node = parser.readValueAsTree();
                JsonNode map = getRequiredNode(node, "/CirrusSearchConcreteNamespaceMap");
                JsonNode clusterGroup = getRequiredNode(node, "/CirrusSearchConcreteReplicaGroup");
                Map<Long, String> nsMap =
                        objectMapper.convertValue(map, new TypeReference<Map<Long, String>>() {});
                return new IndexSettings(wikiId, nsMap, clusterGroup.textValue());
            }
        }
    }

    private URI buildUri(String domain) {
        return URI.create("https://" + domain + API_PATH);
    }

    private CloseableHttpClient getClient() {
        if (client == null) {
            client = HttpClientFactory.buildSyncClient(httpRoutes, httpRequestTimeout);
        }
        return client;
    }

    @Value
    public static class IndexSettings {
        String wikiId;
        Map<Long, String> namespaceMap;
        String cirrusReplica;
    }
}
