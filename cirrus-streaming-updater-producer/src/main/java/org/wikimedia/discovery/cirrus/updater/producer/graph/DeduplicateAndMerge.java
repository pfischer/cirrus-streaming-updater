package org.wikimedia.discovery.cirrus.updater.producer.graph;

import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.Window;
import org.apache.flink.util.Collector;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Deduplicates and merges events from multiple sources.
 *
 * <p>The general goal here is to reduce indexing load on the downstream elasticsearch instances by
 * reducing the set of updates we emit to the fewest possible. In most cases we should be able to
 * emit a single update event per window.
 */
public class DeduplicateAndMerge<W extends Window>
        extends ProcessWindowFunction<InputEvent, InputEvent, Tuple2<String, Long>, W> {
    public static final KeySelector<InputEvent, Tuple2<String, Long>> KEY_SELECTOR =
            new DomainAndPageIdKeySelector();

    private static final long serialVersionUID = -6737513534227247997L;

    private transient Counter deduplicated;
    private transient Counter merged;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        deduplicated = getRuntimeContext().getMetricGroup().counter("deduplicated");
        merged = getRuntimeContext().getMetricGroup().counter("merged");
    }

    @Override
    public void process(
            Tuple2<String, Long> key,
            ProcessWindowFunction<InputEvent, InputEvent, Tuple2<String, Long>, W>.Context context,
            Iterable<InputEvent> in,
            Collector<InputEvent> out) {
        final InputEventMerger merger = InputEventMerger.create(in);
        deduplicated.inc(merger.getDuplicateCount());
        merged.inc(merger.getMergedCount());
        merger.get().ifPresent(out::collect);
    }

    @SuppressFBWarnings(
            value = "IMC_IMMATURE_CLASS_BAD_SERIALVERSIONUID",
            justification = "generated serialVersionUID is considered invalid")
    private static class DomainAndPageIdKeySelector
            implements KeySelector<InputEvent, Tuple2<String, Long>> {

        private static final long serialVersionUID = 4586581120255652100L;

        @Override
        public Tuple2<String, Long> getKey(InputEvent inputEvent) {
            return Tuple2.of(
                    inputEvent.getTargetDocument().getDomain(), inputEvent.getTargetDocument().getPageId());
        }
    }
}
