package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.stream.Collectors.groupingBy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.Update;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.Update.UpdateBuilder;

import com.google.common.base.Verify;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** Accumulator used to reduce a stream of (windowed) events for a single page. */
public class InputEventMerger {

    private static final String UNPREFIXED_PREFIX = "__UNNAMED_GROUPING__";

    /** Sorts events such that the most recent event to the newest revision comes first. */
    private static final Comparator<InputEvent> MOST_RECENT_FIRST =
            Comparator.comparing(InputEvent::getRevId, Comparator.reverseOrder())
                    .thenComparing(InputEvent::getEventTime, Comparator.reverseOrder());

    private int duplicates;

    private final Collection<InputEvent> partials = new ArrayList<>();

    private InputEvent primary;

    public int getDuplicateCount() {
        return duplicates;
    }

    public int getMergedCount() {
        return partials.size() - (primary == null ? 1 : 0);
    }

    /**
     * Deduplicates indexing events
     *
     * <p>This is essentially a bypassable reduce operation. Events that cannot be deduplicated are
     * passed through untouched. Events that can are reduced such that only the last event to the
     * highest revision id is emitted. Bypassed events are added to the list provided in parameters,
     * the reduced primary event, or null, is returned. This has the effect of separating out a single
     * nullable primary event, which typically comes from mediawiki, and a list of secondary tags
     * update events to be merged into the primary event.
     *
     * <p>The general goal here is to deduplicate over the action space. Revision based updates from
     * mediawiki only do two things: 1) delete the page 2) replace the page with results of cirrusdoc
     * api. Doing any of those actions twice in a row would be pointless, the second action either
     * does nothing or replaces what the prior action did. Similarly, these actions are inverse of
     * each other. A delete after an update, or indexing a full doc after deleting it are equally
     * pointless. In all cases the last action overrides the prior.
     */
    public static InputEventMerger create(Iterable<InputEvent> windowEvents) {
        return StreamSupport.stream(windowEvents.spliterator(), false)
                .sorted(MOST_RECENT_FIRST)
                .reduce(
                        new InputEventMerger(),
                        InputEventMerger::merge,
                        InputEventMerger::verifySequentialReduce);
    }

    /**
     * Merges multiple revision based index updates.
     *
     * <p>When receiving edit-based events we will likely have the initial edit ({@link #primary})
     * event along with followup events ({@link #partials}), for example, for weighted tags that are
     * generated by other services based on those same edit events.
     *
     * <p>This process merges the partial updates with the initial update.
     *
     * @return if {@link #primary} is set, an {@link Optional} wrapping {@link #primary} with merged
     *     updates, otherwise an empty {@link Optional}
     */
    public Optional<InputEvent> get() {
        if (primary != null
                && (primary.getChangeType() == ChangeType.PAGE_DELETE || partials.isEmpty())) {
            return Optional.of(primary);
        } else if (primary == null && partials.size() == 1) {
            return partials.stream().findFirst();
        }

        Verify.verify(primary == null || primary.getChangeType() == ChangeType.REV_BASED_UPDATE);

        return Stream.concat(primary != null ? Stream.of(primary) : Stream.empty(), partials.stream())
                .reduce(InputEventMerger::merge);
    }

    /**
     * Combiner to be used with {@link Stream#reduce(Object, BiFunction, BinaryOperator)}.
     *
     * @param self the reduction result
     * @param event the event to be merged in
     * @return {@code self}
     */
    private static InputEventMerger merge(InputEventMerger self, InputEvent event) {
        switch (event.getChangeType()) {
            case TAGS_UPDATE:
            case REDIRECT_UPDATE:
                self.partials.add(event);
                break;
            case PAGE_DELETE:
            case REV_BASED_UPDATE:
                if (self.primary == null) {
                    self.primary = event;
                } else {
                    ++self.duplicates;
                }
                break;
            default:
                throw new UnsupportedOperationException(
                        "Unable to accumulate of type '" + event.getChangeType() + "' (" + event + ")");
        }
        return self;
    }

    /**
     * Combiner to be used with {@link Stream#reduce(Object, BiFunction, BinaryOperator)}.
     *
     * @param primary the primary event to merge in
     * @param partial the partial event to be merged in
     * @return the primary event with merged update
     */
    private static InputEvent merge(InputEvent primary, InputEvent partial) {
        final Update primaryUpdate = primary.getUpdate();
        final Update partialUpdate = partial.getUpdate();
        final UpdateBuilder primaryUpdateBuilder = primaryUpdate.toBuilder();
        mergeNoopHints(primaryUpdate, partialUpdate).ifPresent(primaryUpdateBuilder::noopHints);
        mergeWeightedTags(primaryUpdate, partialUpdate).ifPresent(primaryUpdateBuilder::weightedTags);
        mergeRedirectChange(
                        primaryUpdate,
                        partialUpdate,
                        Update::getRedirectRemove,
                        (redirect) ->
                                primaryUpdate.getRedirectAdd() == null
                                        || !primaryUpdate.getRedirectAdd().contains(redirect))
                .ifPresent(primaryUpdateBuilder::redirectRemove);
        mergeRedirectChange(
                        primaryUpdate,
                        partialUpdate,
                        Update::getRedirectAdd,
                        (redirect) ->
                                primaryUpdate.getRedirectRemove() == null
                                        || !primaryUpdate.getRedirectRemove().contains(redirect))
                .ifPresent(primaryUpdateBuilder::redirectAdd);
        primary.setUpdate(primaryUpdateBuilder.build());
        return primary;
    }

    private static Optional<Map<String, String>> mergeNoopHints(Update primary, Update partial) {
        if (partial.getNoopHints() != null) {
            final Map<String, String> primaryNoopHints = primary.getNoopHints();
            if (primaryNoopHints != null) {
                final Map<String, String> mergedNoopHints = newHashMap(primaryNoopHints);
                mergedNoopHints.putAll(partial.getNoopHints());
                return Optional.of(mergedNoopHints);
            } else {
                return Optional.of(newHashMap(partial.getNoopHints()));
            }
        }
        return Optional.empty();
    }

    private static Optional<Set<String>> mergeWeightedTags(Update primary, Update partial) {
        if (partial.getWeightedTags() != null) {
            if (primary.getWeightedTags() != null) {
                final Map<String, List<String>> mergedWeightedTags =
                        newHashMap(groupWeightedTagsByPrefix(primary.getWeightedTags()));
                groupWeightedTagsByPrefix(partial.getWeightedTags())
                        .forEach(mergedWeightedTags::putIfAbsent);
                return Optional.of(
                        mergedWeightedTags.values().stream()
                                .flatMap(Collection::stream)
                                .collect(Collectors.toSet()));
            } else {
                return Optional.of(newHashSet(partial.getWeightedTags()));
            }
        }
        return Optional.empty();
    }

    private static Optional<Set<TargetDocument>> mergeRedirectChange(
            Update primary,
            Update partial,
            Function<Update, Set<TargetDocument>> extractor,
            Predicate<TargetDocument> addIf) {

        final Set<TargetDocument> partialRedirects = extractor.apply(partial);
        if (partialRedirects != null) {
            final Set<TargetDocument> primaryRedirects = extractor.apply(primary);
            if (primaryRedirects != null) {
                final Set<TargetDocument> mergedRedirectChange = newHashSet(primaryRedirects);
                partialRedirects.stream().filter(addIf).forEach(mergedRedirectChange::add);
                if (mergedRedirectChange.size() > primaryRedirects.size()) {
                    return Optional.of(mergedRedirectChange);
                }
                return Optional.empty();
            }
            return Optional.of(newHashSet(partialRedirects));
        }
        return Optional.empty();
    }

    /**
     * Merge many tags updates into a single update.
     *
     * <p>While the tags are provided as a {@link List List<String>}, when interpreted by the
     * downstream system the tags are grouped by prefix into {@link Map Map<String,List<String>}. When
     * applying sequential tags updates the last update to a prefix always overwrites prior updates,
     * meaning each prefix in the merged result must come from a single event, and that event must be
     * the most recent event.
     *
     * @param weightedTags a collection of tags
     * @return a map grouping {@code weightedTags} by their prefix
     */
    private static Map<String, List<String>> groupWeightedTagsByPrefix(
            Collection<String> weightedTags) {
        return weightedTags.stream().collect(groupingBy(InputEventMerger::weightedTagPrefix));
    }

    private static String weightedTagPrefix(String tag) {
        int pos = tag.indexOf('/');
        if (pos == -1) {
            // These shouldn't exist in modern streams, but the downstream superdetectnoop plugin
            // accepts un-prefixed tags.
            return UNPREFIXED_PREFIX;
        }
        return tag.substring(0, pos);
    }

    /**
     * Method to be referenced as {@code combiner} in {@link Stream#reduce(Object,
     * java.util.function.BiFunction, java.util.function.BinaryOperator)}.
     *
     * <p>Since we run in a single-threaded environment (inside k8s), we do not use parallel stream
     * processing and therefore, the {@code combiner} never gets called.
     */
    @SuppressFBWarnings("UP_UNUSED_PARAMETER")
    private static <T> T verifySequentialReduce(T identityA, T identityB) {
        throw new UnsupportedOperationException("must be sequential/non-parallel");
    }
}
