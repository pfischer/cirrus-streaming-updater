package org.wikimedia.discovery.cirrus.updater.common.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.flink.types.Row;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

import com.google.common.collect.ImmutableMap;

class UpdateFieldsTest {

    @Test
    void read() {
        final EventRowTypeInfo typeInfo = EventDataStreamUtilities.buildUpdateTypeInfo();
        final Row emptyRow = typeInfo.createEmptyRow();

        final Row fields = typeInfo.createEmptySubRow("fields");

        emptyRow.setField("page_id", 42L);
        emptyRow.setField("rev_id", 7L);
        emptyRow.setField("operation", "test_operation");
        emptyRow.setField("index_name", "test_index");
        emptyRow.setField("noop_hints", ImmutableMap.of("test_key", "test_value"));
        emptyRow.setField("fields", fields);

        assertThat(UpdateFields.getPageId(emptyRow)).isEqualTo(42L);
        assertThat(UpdateFields.getRevId(emptyRow)).isEqualTo(7L);
        assertThat(UpdateFields.getOperation(emptyRow)).isEqualTo("test_operation");
        assertThat(UpdateFields.getIndexName(emptyRow)).isEqualTo("test_index");
        assertThat(UpdateFields.getNoopHints(emptyRow))
                .isEqualTo(ImmutableMap.of("test_key", "test_value"));
        assertThat(UpdateFields.getFields(emptyRow)).isEqualTo(fields);
    }
}
