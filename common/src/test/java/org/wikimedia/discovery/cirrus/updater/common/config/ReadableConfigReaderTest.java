package org.wikimedia.discovery.cirrus.updater.common.config;

import java.util.Collections;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.configuration.ConfigOptions;
import org.apache.flink.configuration.ReadableConfig;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableMap;

class ReadableConfigReaderTest {

    @Test
    void required_missing() {
        ReadableConfig config = ParameterTool.fromMap(Collections.emptyMap()).getConfiguration();
        final ConfigOption<String> required =
                ConfigOptions.key("required").stringType().noDefaultValue();

        Assertions.assertThatIllegalArgumentException()
                .isThrownBy(() -> ReadableConfigReader.getRequired(config, required))
                .withNoCause();
    }

    @Test
    void required_present() {
        ReadableConfig config =
                ParameterTool.fromMap(ImmutableMap.of("required", "0")).getConfiguration();
        final ConfigOption<String> required =
                ConfigOptions.key("required").stringType().noDefaultValue();

        Assertions.assertThatNoException()
                .isThrownBy(() -> ReadableConfigReader.getRequired(config, required));
    }
}
