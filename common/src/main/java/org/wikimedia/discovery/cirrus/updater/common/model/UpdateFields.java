package org.wikimedia.discovery.cirrus.updater.common.model;

import static com.google.common.collect.Maps.newHashMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.flink.api.java.typeutils.RowTypeInfo;
import org.apache.flink.types.Row;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowSerializationSchema;

import com.google.common.collect.ImmutableMap;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
public class UpdateFields {

    /** Partial update of a page. If it does not exist do nothing. */
    public static final String OPERATION_PARTIAL_UPDATE = "update_existing";

    /** Full update of a page. If it does not exist create it with the contained fields. */
    public static final String OPERATION_UPDATE_REVISION = "update_revision";
    /** Remove a page from the search index. */
    public static final String OPERATION_DELETE = "delete";

    public static final String PAGE_ID = "page_id";
    public static final String REV_ID = "rev_id";
    public static final String FIELDS = "fields";
    public static final String OPERATION = "operation";
    public static final String PAGE_NAMESPACE = "page_namespace";
    public static final String NOOP_HINTS = "noop_hints";
    public static final String NOOP_FIELDS = "noop_fields";
    public static final String INDEX_NAME = "index_name";
    public static final String CLUSTER_GROUP = "cluster_group";
    public static final String WEIGHTED_TAGS = "weighted_tags";
    public static final String REDIRECT_NAMESPACE = "namespace";
    public static final String REDIRECT_TITLE = "title";
    public static final String REDIRECT = "redirect";
    public static final String NOOP_HINTS_SET_ADD = "add";
    public static final String NOOP_HINTS_SET_REMOVE = "remove";
    public static final String NOOP_HINTS_SET_MAX_SIZE = "max_size";

    public static Row getFields(Row updateEvent) {
        return updateEvent.getFieldAs(FIELDS);
    }

    public static String getIndexName(Row updateEvent) {
        return updateEvent.getFieldAs(INDEX_NAME);
    }

    public static Map<String, String> getNoopHints(Row updateEvent) {
        return updateEvent.getFieldAs(NOOP_HINTS);
    }

    public static String getOperation(Row updateEvent) {
        return updateEvent.getFieldAs(OPERATION);
    }

    public static Long getPageId(Row row) {
        return row.getFieldAs(PAGE_ID);
    }

    public static Long getRevId(Row row) {
        return row.getFieldAs(REV_ID);
    }

    public static JsonRowSerializationSchema buildFieldsRowSerializer(RowTypeInfo updateTypeInfo) {
        return JsonRowSerializationSchema.builder()
                .withTypeInfo(updateTypeInfo.getTypeAt(UpdateFields.FIELDS))
                .withoutNormalization()
                .build();
    }

    public static Optional<NoopSetParameters<Map<String, Object>>> getRedirectNoopSetParameters(
            Row updateEvent) {
        return Optional.ofNullable(updateEvent.<Row>getFieldAs(NOOP_FIELDS))
                .map(noopFields -> noopFields.<Row>getFieldAs(REDIRECT))
                .map(
                        noopSetRedirectParameters -> {
                            final NoopSetParameters<Map<String, Object>> parameters = new NoopSetParameters<>();

                            decodeRedirects(noopSetRedirectParameters, NOOP_HINTS_SET_ADD)
                                    .ifPresent(parameters::setAdd);
                            decodeRedirects(noopSetRedirectParameters, NOOP_HINTS_SET_REMOVE)
                                    .ifPresent(parameters::setRemove);

                            Optional.ofNullable(
                                            noopSetRedirectParameters.<Number>getFieldAs(NOOP_HINTS_SET_MAX_SIZE))
                                    .map(Number::intValue)
                                    .ifPresent(parameters::setMaxSize);
                            return parameters;
                        });
    }

    private static Optional<List<Map<String, Object>>> decodeRedirects(
            Row noopSetRedirectParameters, String noopSetParameter) {
        return Optional.ofNullable(noopSetRedirectParameters.<Row[]>getFieldAs(noopSetParameter))
                .map(
                        redirects ->
                                Stream.of(redirects).map(UpdateFields::decodeRedirect).collect(Collectors.toList()))
                .filter(list -> !list.isEmpty());
    }

    private static Map<String, Object> decodeRedirect(Row row) {
        return ImmutableMap.of(
                REDIRECT_NAMESPACE, row.getFieldAs(REDIRECT_NAMESPACE),
                REDIRECT_TITLE, row.getFieldAs(REDIRECT_TITLE));
    }

    @Getter
    @Setter
    public static class NoopSetParameters<T> {

        List<T> add;
        List<T> remove;
        Integer maxSize;

        public Map<String, Object> toMap() {
            final HashMap<String, Object> map = newHashMap();
            if (add != null) {
                map.put(UpdateFields.NOOP_HINTS_SET_ADD, add);
            }
            if (remove != null) {
                map.put(UpdateFields.NOOP_HINTS_SET_REMOVE, remove);
            }
            if (maxSize != null) {
                map.put(UpdateFields.NOOP_HINTS_SET_MAX_SIZE, maxSize);
            }
            return map;
        }
    }
}
